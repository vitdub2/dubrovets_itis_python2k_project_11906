from django.contrib.auth import get_user_model

# Create your views here.
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, serializers
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

User = get_user_model()


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'name', 'surname', 'telegram']


class UserList(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Get all users",
        responses={200: UserSerializer(many=True)}
    )
    def get(self, request, format=None):
        snippets = User.objects.all()
        serializer = UserSerializer(snippets, many=True)
        return Response(serializer.data)


class UserDetail(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Get users by id",
        responses={
            200: UserSerializer(many=True),
            404: "Not found"
        },
        manual_parameters=[
            openapi.Parameter(
                "id",
                openapi.IN_PATH,
                description="User ID",
                type=openapi.TYPE_INTEGER
            )
        ]
    )
    def get(self, request, id, format=None):
        user = User.objects.get(id=id)
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)


class UserByEmail(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Get users by email",
        responses={
            200: UserSerializer(many=True),
            404: "Not found"
        },
        manual_parameters=[
            openapi.Parameter(
                "email",
                openapi.IN_PATH,
                description="User`s e-mail",
                type=openapi.TYPE_STRING
            )
        ]
    )
    def get(self, request, email, format=None):
        user = User.objects.filter(email=email)
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)


class UserByTelegram(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Get users by telegram",
        responses={
            200: UserSerializer(many=True),
            404: "Not found"
        },
        manual_parameters=[
            openapi.Parameter(
                "telegram",
                openapi.IN_PATH,
                description="User`s telegram",
                type=openapi.TYPE_STRING
            )
        ]
    )
    def get(self, request, telegram, format=None):
        user = User.objects.filter(telegram=telegram)
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)