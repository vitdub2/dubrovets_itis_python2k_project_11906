from django.conf.urls import url
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

schema_view = get_schema_view(
   openapi.Info(
      title="Memorio API",
      default_version='v1',
      description="Memorio API Documentation",
      contact=openapi.Contact(email="vitdub2@gmail.com"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

app_name = 'api'
urlpatterns = [
    path('users', views.UserList.as_view()),
    path('users/<int:id>', views.UserDetail.as_view()),
    re_path(r'^users/byEmail/(?P<email>[a-zA-Z@.0-9]{0,50})$', views.UserByEmail.as_view()),
    re_path(r'^users/byTelegram/(?P<telegram>[a-zA-Z@.0-9]{0,50})$', views.UserByTelegram.as_view()),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('', schema_view.with_ui('swagger', cache_timeout=0)),
]