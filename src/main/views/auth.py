import random
import uuid

from django.contrib.auth import authenticate, login, logout, get_user_model
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import FormView

from main.forms import AuthForm, SignUpForm, PasswordResetForm, ConfirmedPasswordResetForm
from main.mixins import BaseLoginMixin
from background.tasks import send_reset_password_mail


def generate_reset_code():
    random_48_bits = random.randint(0, 2 ** 48 - 1)
    reset_code = uuid.uuid1(random_48_bits).__str__().upper()
    return reset_code[0:4]


Account = get_user_model()


class HomeView(BaseLoginMixin, View):
    template_name = "main.html"

    def get(self, request):
        account = request.user
        return render(request, self.template_name, {
            "fullname": ('%s %s' % (account.name, account.surname)),
            "avatar": account.photo,
        })


class AuthView(FormView):
    template_name = 'auth.html'
    form_class = AuthForm
    success_url = 'main:home'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            return redirect('main:home')
        form = AuthForm()
        return render(request, self.template_name, {'form': form})

    def form_valid(self, form):
        req = self.request

        email = req.POST['email']
        password = req.POST['password']

        user = authenticate(req, email=email, password=password)
        if user is not None:
            login(req, user)
            return redirect('main:home')
        else:
            form = AuthForm()
            return render(req, self.template_name, {'form': form})


def logout_view(req):
    logout(req)
    return redirect('main:auth')


class ConfirmedPasswordResetView(FormView):
    template_name = 'confirmed_password_reset.html'
    form_class = ConfirmedPasswordResetForm
    success_url = 'main:home'
    code = ''

    def get_context_data(self, **kwargs):
        self.code = self.kwargs['code']

    def get(self, request, *args, **kwargs):
        form = ConfirmedPasswordResetForm()
        self.code = self.kwargs['code']
        return render(request, self.template_name, {'code': self.code, 'form': form})

    def form_valid(self, form):
        User = get_user_model()
        req = self.request
        user = User.objects.filter(password_reset_code=self.kwargs['code']).first()
        if user is not None and req.POST['password'] is not None:
            password = req.POST['password']
            user.set_password(password)
            user.save()
            login(req, user)
            return redirect('main:home')
        else:
            return redirect('main:auth')


class ResetPasswordView(FormView):
    template_name = 'password_reset.html'
    form_class = PasswordResetForm
    success_url = 'main:auth'

    def form_valid(self, form):
        User = get_user_model()
        req = self.request
        email = req.POST['email']
        user = User.objects.filter(email=email).first()
        if user is not None:
            site = req.build_absolute_uri('/reset_password')
            reset_code = generate_reset_code()
            User = get_user_model()
            user = User.objects.filter(email=email).first()
            user.password_reset_code = reset_code
            user.save()
            send_reset_password_mail.delay(reset_code, email, site)
            return redirect('main:auth')
        else:
            return redirect('main:reset')


class SignUpView(FormView):
    template_name = 'sign_up.html'
    form_class = SignUpForm
    success_url = 'main:home'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            return redirect('main:home')
        form = SignUpForm()
        return render(request, self.template_name, {'form': form})

    def form_valid(self, form):
        req = self.request
        account = Account.objects.create_user(
            name=req.POST['name'],
            surname=req.POST['surname'],
            password=req.POST['password'],
            email=req.POST['email'],
            telegram=req.POST['telegram'],
            photo=req.FILES['photo']
        )
        account.save()
        return redirect('main:home')


class SettingsView(BaseLoginMixin, View):
    template_name = 'settings.html'

    def get(self, req):
        return render(req, self.template_name)
