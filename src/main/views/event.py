from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import ListView, FormView

from main import services, models
from main.forms import EventForm
from main.mixins import BaseLoginMixin
from main.models import EventInfo, Event, Calendar


class CreateEventView(BaseLoginMixin, FormView):
    template_name = 'event_create.html'
    form_class = EventForm

    def get_context_data(self, **kwargs):
        calendar_id = self.kwargs['calendar_id']
        context = super().get_context_data(**kwargs)
        calendar = Calendar.objects.get(pk=calendar_id)
        context['calendar'] = calendar
        context['calendar_id'] = calendar_id
        return context

    def form_valid(self, form):
        req = self.request
        account = req.user
        calendar_id = self.kwargs['calendar_id']
        calendar = models.Calendar.objects.get(id=calendar_id)
        editor = services.get_user_with_specified_role(account.id, 'ED')
        event_info = models.EventInfo(
            name=req.POST['name'],
            start_time=req.POST['start_time'],
            end_time=req.POST['end_time'],
            description=req.POST['description'],
            place=req.POST['place'],
            color=req.POST['color'],
            site=req.POST['site'],
            # repeat_interval=req.POST['repeat_interval'],
            # status=req.POST['status'],
        )
        event_info.save()

        event = models.Event(
            event_info=event_info,
            created_by=editor,
        )
        event.save()

        calendar.events.add(event)

        return redirect('main:calendar_info', calendar_id=calendar_id)


class EditEventView(BaseLoginMixin, FormView):
    template_name = 'event_edit.html'
    form_class = EventForm

    def get_context_data(self, **kwargs):
        event_id = self.kwargs['event_id']
        calendar_id = self.kwargs['calendar_id']
        event = Event.objects.get(pk=event_id).event_info
        context = super().get_context_data(**kwargs)
        form = EventForm(initial={
            'name': event.name,
            'start_time': event.start_time,
            'end_time': event.start_time,
            'description': event.description,
            'place': event.place,
            'color': event.color,
            'site': event.site,
            # 'repeat_interval': event.repeat_interval,
            # 'status': event.status
        })
        context['calendar_id'] = calendar_id
        context['event_id'] = event_id
        context['form'] = form
        return context

    def form_valid(self, form):
        req = self.request
        calendar_id = self.kwargs['calendar_id']
        event_id = self.kwargs['event_id']
        event = Event.objects.get(pk=event_id).event_info

        event.name = req.POST['name']
        event.start_time = req['start_time']
        event.end_time = req.POST['end_time']
        event.description = req.POST['description']
        event.place = req.POST['place']
        event.color = req.POST['color']
        event.site = req.POST['site']
        # event.repeat_interval = req.POST['repeat_interval']
        # event.status = req.POST['status']
        event.save()

        return redirect('main:calendars')


class EventListView(BaseLoginMixin, ListView):
    model = EventInfo
    template_name = 'events.html'

    def get_context_data(self, **kwargs):
        req = self.request
        account = req.user
        context = super().get_context_data(**kwargs)
        events = services.get_all_events_by_account(account.id)
        context['events'] = events
        context['now'] = datetime.now()
        return context


class EventInfoView(BaseLoginMixin, ListView):
    template_name = 'event_info.html'
    model = EventInfo

    def get_context_data(self, **kwargs):
        req = self.request
        context = super().get_context_data(**kwargs)
        calendar_id = self.kwargs['calendar_id']
        event_info_id = self.kwargs['event_id']
        event_info = EventInfo.objects.get(pk=event_info_id)
        event = Event.objects.filter(event_info=event_info).first()
        context['calendar_id'] = calendar_id
        context['event_id'] = event.id
        context['event'] = event.event_info
        return context


@login_required(login_url='/auth')
def delete_event(req, calendar_id, event_id):
    calendar = Calendar.objects.get(pk=calendar_id)
    event = Event.objects.get(pk=event_id)
    calendar.events.remove(event)
    event.event_info.delete()
    event.delete()
    calendar.save()
    return redirect('main:calendar_info', calendar_id=calendar_id)