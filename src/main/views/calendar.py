from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import ListView, FormView

from main import services, models
from main.forms import CalendarCreateForm
from main.mixins import BaseLoginMixin
from main.models import Calendar


class CalendarListView(BaseLoginMixin, ListView):
    model = Calendar
    template_name = 'calendars.html'

    def get_context_data(self, **kwargs):
        req = self.request
        account = req.user
        context = super().get_context_data(**kwargs)
        calendars = services.get_all_calendars_for_user(account.id)
        context['calendars'] = calendars
        return context


@login_required(login_url='/auth')
def calendar_info(req, calendar_id):
    is_allowed = services.is_account_allowed_to_access_calendar(req.user.id, calendar_id)
    if is_allowed:
        calendar = models.Calendar.objects.get(id=calendar_id)
        events = services.get_all_events_by_calendar_id(calendar_id)
        is_editor = services.is_account_editor_for_calendar(calendar.id, req.user.id)
        return render(req, 'calendar_info.html', {
            'events': events,
            'calendar': calendar,
            'is_editor': is_editor
        })
    else:
        return redirect("main:home")


class CreateCalendarView(BaseLoginMixin, FormView):
    template_name = 'calendar_create.html'
    form_class = CalendarCreateForm

    def form_valid(self, form):
        req = self.request
        user = req.user
        editor = services.get_user_with_specified_role(user.id, 'ED')
        if editor is None:
            editor = models.CalendarUser(account=user, roles='ED')
            editor.save()
        new_calendar = models.Calendar(name=req.POST['name'])
        new_calendar.save()
        new_calendar.users.set([editor])
        new_calendar.save()
        return redirect('main:calendars')