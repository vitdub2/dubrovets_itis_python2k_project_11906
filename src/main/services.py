from . import models


def get_all_events_by_calendar_id(calendar_id):
    event_ids = models.Calendar \
        .objects \
        .filter(id=calendar_id) \
        .values('events__id')

    return models.EventInfo.objects.filter(event__id__in=event_ids)


def get_all_calendars_for_user(user_id):
    return models.Calendar.objects.filter(users__account__id=user_id)


def is_account_editor_for_calendar(calendar_id, account_id):
    editors = models.CalendarUser.objects.filter(account_id=account_id, roles='ED')
    calendar = models.Calendar.objects.filter(id=calendar_id, users__in=editors)
    return calendar.count() > 0


def get_all_events_by_user(user_id):
    events = models.Calendar \
        .objects \
        .filter(users__account_id__exact=user_id) \
        .values('events__id') \
        .prefetch_related('event_info_set') \
        .all()
    return events


def get_all_events_by_account(account_id):
    users = models.CalendarUser \
        .objects \
        .filter(account_id=account_id) \
        .values('id')
    event_ids = models.Calendar \
        .objects \
        .filter(users__id__in=users) \
        .values('events__id')
    events = models.EventInfo.objects.filter(event__id__in=event_ids)
    return events


def get_user_with_specified_role(account_id, role):
    return models.CalendarUser.objects.filter(account_id=account_id, roles=role).first()


def is_account_has_user_with_role(account_id, role):
    users = models.CalendarUser.objects.filter(account_id=account_id, roles=role)
    return users.count() > 0


def is_account_allowed_to_access_calendar(account_id, calendar_id):
    calendar = models.Calendar.objects.filter(id=calendar_id)
    return calendar.filter(users__account_id=account_id).count() > 0