from django.contrib.auth.mixins import LoginRequiredMixin

from memorio import settings


class BaseLoginMixin(LoginRequiredMixin):
    login_url = settings.LOGIN_URL
