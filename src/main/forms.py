from datetime import datetime

from django import forms

from main.models import EventInfo, Label


class AuthForm(forms.Form):
    email = forms.EmailField(label='E-mail', max_length=100, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'uk-input'}))


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label='E-mail', max_length=100, widget=forms.TextInput(attrs={'class': 'uk-input'}))


class ConfirmedPasswordResetForm(forms.Form):
    password = forms.CharField(label='Password', max_length=100, widget=forms.TextInput(attrs={'class': 'uk-input'}))


class SignUpForm(forms.Form):
    email = forms.EmailField(label='E-mail', max_length=100, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'uk-input'}))
    name = forms.CharField(label='Name', max_length=20, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    surname = forms.CharField(label='Surname', max_length=20, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    telegram = forms.CharField(label='Telegram', max_length=60, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    photo = forms.FileField(label='Photo', widget=forms.FileInput(attrs={'class': 'uk-input'}))


class CalendarCreateForm(forms.Form):
    name = forms.CharField(label='Calendar name', max_length=50, widget=forms.TextInput(attrs={'class': 'uk-input'}))


class EventForm(forms.Form):
    name = forms.CharField(label='Event name', widget=forms.TextInput(attrs={'class': 'uk-input'}))
    start_time = forms.DateTimeField(label='Start time', initial=datetime.today(), widget=forms.TextInput(attrs={'class': 'uk-input'}))
    end_time = forms.DateTimeField(label='End time', initial=datetime.today(), widget=forms.TextInput(attrs={'class': 'uk-input'}))
    description = forms.CharField(label='Description', required=False, widget=forms.Textarea(attrs={'class': 'uk-input'}))
    place = forms.CharField(label='Place', required=False, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    color = forms.CharField(label='Color', required=False, widget=forms.TextInput(attrs={'class': 'uk-input'}))
    site = forms.URLField(label='Site', required=False, widget=forms.TextInput(attrs={'class': 'uk-input'}))

    REPEAT_INTERVAL = [
        ('NONE', 'None'),
        ('DAY', 'Every day'),
        ('WEEK', 'Every week'),
        ('MONTH', 'Every month'),
        ('YEAR', 'Every year')
    ]

    # repeat_interval = forms.MultipleChoiceField(
    #     required=False,
    #     widget=forms.Select,
    #     choices=REPEAT_INTERVAL,
    #     label='Repeat interval'
    # )

    EVENT_STATUSES = [
        ('NOT_STARTED', 'Not started'),
        ('PROCESS', 'In process'),
        ('COMPLETED', 'Completed'),
        ('DELAYED', 'Delayed')
    ]

    # status = forms.MultipleChoiceField(
    #     required=False,
    #     widget=forms.Select,
    #     choices=EVENT_STATUSES,
    #     label='Status'
    # )

    attachments = forms.FileInput()

    labels = forms.ModelMultipleChoiceField(
        queryset=Label.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label='Labels'
    )
