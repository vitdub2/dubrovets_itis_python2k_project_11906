import os
from datetime import datetime, timezone

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from main.models import CalendarUser, Calendar, EventInfo, Event


class ViewTests(TestCase):
    def setUp(self):
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "memorio.settings")
        User = get_user_model()
        self.email = 'vitdub2@gmail.com'
        self.password = '1234'

        user = User.objects.create_user(self.email, self.password)
        user.photo = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/2048px-Google_%22G%22_Logo.svg.png'
        user.save()

        calendar_user = CalendarUser(account=user)
        calendar_user.roles = 'ED'
        calendar_user.save()

        calendar = Calendar(name='Calendar 1')
        calendar.save()
        calendar.users.add(calendar_user)

        event_info = EventInfo(name='Event 1', start_time=datetime.now(tz=timezone.utc), end_time=datetime.now(tz=timezone.utc))
        event_info.save()

        event = Event(created_by=calendar_user, event_info=event_info)
        event.save()

        calendar.events.add(event)

        calendar.save()

        self.user = user
        self.client = Client()

    def authorizate(self):
        self.client.login(email=self.email, password=self.password)

    def test_home_view_redirects(self):
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 302)

        self.authorizate()
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_auth_and_sign_up_redirects(self):
        response = self.client.get(reverse('main:auth'))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('main:sign_up'))
        self.assertEqual(response.status_code, 200)

        self.authorizate()

        response = self.client.get(reverse('main:auth'))
        self.assertEqual(response.status_code, 302)

        response = self.client.get(reverse('main:sign_up'))
        self.assertEqual(response.status_code, 302)