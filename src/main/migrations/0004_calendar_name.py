# Generated by Django 3.1.7 on 2021-03-14 19:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20210314_2208'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendar',
            name='name',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
