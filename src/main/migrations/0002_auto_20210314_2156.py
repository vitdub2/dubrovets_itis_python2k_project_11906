# Generated by Django 3.1.7 on 2021-03-14 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='account',
            name='surname',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='account',
            name='telegram',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='eventinfo',
            name='color',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='eventinfo',
            name='name',
            field=models.CharField(max_length=60),
        ),
        migrations.AlterField(
            model_name='eventinfo',
            name='place',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='label',
            name='color',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='label',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]
