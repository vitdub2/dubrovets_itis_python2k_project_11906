# Generated by Django 3.1.7 on 2021-03-16 21:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_auto_20210316_1907'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventinfo',
            options={'verbose_name': 'информация', 'verbose_name_plural': 'информация'},
        ),
    ]
