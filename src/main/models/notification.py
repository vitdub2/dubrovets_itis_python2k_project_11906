from django.contrib.auth import get_user_model
from django.db import models

from main.models.base import BaseModel
from main.models.event import Event

User = get_user_model()


class Notification(BaseModel):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, verbose_name='Event')
    message = models.TextField(verbose_name='Message', blank=True)
    last_notify = models.DateTimeField(auto_now_add=True, verbose_name='Last notify date')

    class Meta:
        verbose_name = 'notification'
        verbose_name_plural = 'notifications'

    def __str__(self):
        return self.event.__str__()
