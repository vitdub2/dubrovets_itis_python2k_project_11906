from django.contrib.auth import get_user_model
from django.db import models

from main.models import CalendarUser, File
from main.models.base import BaseModel
from main.models.event import Event

User = get_user_model()


class Comment(BaseModel):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, verbose_name='Event')
    author = models.ForeignKey(CalendarUser, on_delete=models.CASCADE, verbose_name='Author')
    text = models.TextField(verbose_name='Text')
    files = models.ForeignKey(File, on_delete=models.CASCADE, verbose_name='Attachments', blank=True)

    class Meta:
        verbose_name = 'comment'
        verbose_name_plural = 'comments'

    def __str__(self):
        return self.text
