from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import models

from main.models.label import Label
from main.models.base import BaseModel

from django.utils.translation import gettext_lazy as _

from main.models.file import File

User = get_user_model()


class EventInfo(BaseModel):
    name = models.CharField(max_length=60, verbose_name='Event name')
    start_time = models.DateTimeField(verbose_name='Start time')
    end_time = models.DateTimeField(verbose_name='End time')
    description = models.TextField(verbose_name='Description', blank=True)
    place = models.CharField(max_length=100, verbose_name='Place', blank=True)
    color = models.CharField(max_length=20, verbose_name='Color', blank=True)
    site = models.URLField(verbose_name='Site', blank=True)

    class RepeatInterval(models.TextChoices):
        NONE = 'NONE', _('None')
        EVERY_DAY = 'DAY', _('Every day')
        EVERY_WEEK = 'WEEK', _('Every week')
        EVERY_MONTH = 'MONTH', _('Every month')
        EVERY_YEAR = 'YEAR', _('Every year')

    repeat_interval = models.CharField(
        max_length=5,
        choices=RepeatInterval.choices,
        default=RepeatInterval.NONE,
        verbose_name='Repeat interval',
        blank=True
    )

    class EventStatus(models.TextChoices):
        NOT_STARTED = 'NOT_STARTED', _('Not started')
        IN_PROCESS = 'PROCESS', _('In process')
        COMPLETED = 'COMPLETED', _('Completed')
        DELAYED = 'DELAYED', _('Delayed')

    status = models.CharField(
        max_length=11,
        choices=EventStatus.choices,
        default=EventStatus.NOT_STARTED,
        verbose_name='Status',
        blank=True
    )

    attachments = models.ManyToManyField(File, verbose_name='Attachments', blank=True)

    labels = models.ManyToManyField(Label, verbose_name='Labels', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'info'
        verbose_name_plural = 'infos'
