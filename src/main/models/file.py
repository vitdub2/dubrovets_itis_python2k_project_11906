from django.contrib.auth import get_user_model
from django.db import models

from main.models.base import BaseModel

User = get_user_model()


class File(BaseModel):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Author')
    source = models.FileField(upload_to='files', verbose_name='File')

    class Meta:
        verbose_name = 'file'
        verbose_name_plural = 'files'

    def __str__(self):
        return self.source.name
