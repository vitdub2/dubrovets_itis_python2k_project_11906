from django.contrib.auth import get_user_model
from django.db import models

from main.models import CalendarUser
from main.models.base import BaseModel
from main.models.event import Event

User = get_user_model()


class Calendar(BaseModel):
    events = models.ManyToManyField(Event, verbose_name='Event', related_name='events', blank=True)
    users = models.ManyToManyField(CalendarUser, verbose_name='Users', related_name='users')
    name = models.CharField(max_length=100, verbose_name='Name')

    class Meta:
        verbose_name = 'calendar'
        verbose_name_plural = 'calendars'

    def __str__(self):
        return self.name