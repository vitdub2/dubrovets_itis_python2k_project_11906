from django.contrib.auth import get_user_model
from django.db import models

from main.models.base import BaseModel

from django.utils.translation import gettext_lazy as _

User = get_user_model()


class CalendarUser(BaseModel):
    account = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Account')

    class UserRoles(models.TextChoices):
        EDITOR = 'ED', _('Editor')
        PARTICIPANT = 'PT', _('Participant')

    roles = models.CharField(
        max_length=2,
        choices=UserRoles.choices,
        default=UserRoles.PARTICIPANT,
        verbose_name='Role'
    )

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'user'

    def __str__(self):
        return "%s, %s" % (self.account.__str__(), self.roles)
