from django.contrib.auth import get_user_model
from django.db import models
from main.models import BaseModel

User = get_user_model()


class Label(BaseModel):
    name = models.CharField(max_length=30, verbose_name='Name')
    color = models.CharField(max_length=20, verbose_name='Color', blank=True)

    class Meta:
        verbose_name = 'label'
        verbose_name_plural = 'labels'

    def __str__(self):
        return self.name
