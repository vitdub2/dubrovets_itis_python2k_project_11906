from django.contrib.auth import get_user_model
from django.db import models

from main.models import EventInfo
from main.models import CalendarUser
from main.models.base import BaseModel

User = get_user_model()


class Event(BaseModel):
    created_by = models.ForeignKey(CalendarUser, on_delete=models.CASCADE, verbose_name="Created by")
    event_info = models.OneToOneField(EventInfo, on_delete=models.CASCADE, verbose_name='Event info')

    class Meta:
        verbose_name = 'event'
        verbose_name_plural = 'event'

    def __str__(self):
        return self.event_info.__str__()
