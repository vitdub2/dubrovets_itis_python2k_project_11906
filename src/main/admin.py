from django.contrib import admin

from main.models import *


@admin.register(CalendarUser)
class AdminCalendarUser(admin.ModelAdmin):
    list_display = ("account", "roles")
    list_filter = ("roles",)

    sortable_by = ("account__name", "account__surname", "roles")

    search_fields = ("account__name", "account__surname", "roles")


@admin.register(File)
class AdminFile(admin.ModelAdmin):
    list_display = ("author", "source")

    sortable_by = ("account__name", "account__surname")

    search_fields = ("author__name", "author__surname")


@admin.register(Label)
class AdminLabel(admin.ModelAdmin):
    list_display = ("name", "color")

    sortable_by = ("name", "color")

    search_fields = list_display


@admin.register(EventInfo)
class AdminEventInfo(admin.ModelAdmin):
    sortable_by = list_display = ("name", "description", "place", "start_time", "end_time")

    search_fields = list_display


@admin.register(Event)
class AdminEvent(admin.ModelAdmin):
    list_display = ("event_info",)


@admin.register(Notification)
class AdminNotification(admin.ModelAdmin):
    list_display = ("event", "message")

    search_fields = ("message",)

    sortable_by = ("event", "message")


@admin.register(Calendar)
class AdminCalendar(admin.ModelAdmin):
    list_display = ("name",)

    sortable_by = search_fields = list_display


@admin.register(Comment)
class AdminComment(admin.ModelAdmin):
    sortable_by = list_display = ("event", "author", "text")

    search_fields = ("text__contains",)
