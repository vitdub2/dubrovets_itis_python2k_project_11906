from django.urls import path, re_path

from . import views
from .views import HomeView, AuthView, SignUpView, SettingsView, CalendarListView, CreateEventView, CreateCalendarView, \
    ResetPasswordView, ConfirmedPasswordResetView
from .views.event import EventListView, EventInfoView, EditEventView

app_name = 'main'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('auth', AuthView.as_view(), name='auth'),
    path('logout', views.logout_view, name='logout'),
    path('sign_up', SignUpView.as_view(), name='sign_up'),
    path('reset_password/<str:code>/', ConfirmedPasswordResetView.as_view(), name='reset_confirmed'),
    path('reset', ResetPasswordView.as_view(), name='reset'),
    path('calendars', CalendarListView.as_view(), name='calendars'),
    path('calendars/add', CreateCalendarView.as_view(), name='add_calendar'),
    path('calendars/<int:calendar_id>/', views.calendar_info, name='calendar_info'),
    path('calendars/<int:calendar_id>/add_event', CreateEventView.as_view(), name='add_event'),
    path('calendars/<int:calendar_id>/events/<int:event_id>', EventInfoView.as_view(), name='event_info'),
    path('calendars/<int:calendar_id>/events/<int:event_id>/edit', EditEventView.as_view(), name='event_edit'),
    path('calendars/<int:calendar_id>/events/<int:event_id>/delete', views.delete_event, name='event_delete'),
    path('events', EventListView.as_view(), name='events'),
    path('settings', SettingsView.as_view(), name='settings'),
]
