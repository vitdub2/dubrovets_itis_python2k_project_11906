from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.http import urlencode

from main.models import CalendarUser
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import Account


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = Account
    list_display = ('email', 'is_staff', 'is_active', 'name', 'surname', 'telegram', 'show_users')
    list_filter = ('is_staff', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password', 'name', 'surname', 'telegram', 'photo')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active', 'name', 'surname', 'telegram', 'photo')}
        ),
    )
    search_fields = ('email', 'is_staff', 'is_active', 'name', 'surname', 'telegram')
    ordering = ('email',)

    def show_users(self, obj):
        users_count = CalendarUser.objects.filter(account__id=obj.id).count()
        url = (
            reverse("admin:main_calendaruser_changelist")
            + "?"
            + urlencode({"account__id": f"{obj.id}"})
        )
        return format_html('<a href="{}">{} users</a>', url, users_count)

    show_users.short_description = "Users"


admin.site.register(Account, CustomUserAdmin)