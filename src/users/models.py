from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone

from .managers import CustomUserManager


class Account(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='E-mail', unique=True)
    name = models.CharField(max_length=20, verbose_name='Name', default='')
    surname = models.CharField(max_length=20, verbose_name='Surname', default='')
    telegram = models.CharField(max_length=30, verbose_name='Telegram', default='')
    photo = models.ImageField(verbose_name='Photo', max_length=1000)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    password_reset_code = models.CharField(max_length=4, null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = 'account'
        verbose_name_plural = 'accounts'

    def __str__(self):
        return self.email
