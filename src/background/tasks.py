from django.contrib.auth import get_user_model
from django.core.mail import send_mail

from background.celery_app import app
from main.models import CalendarUser, Calendar
from main.services import get_all_events_by_account

User = get_user_model()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(5.0, send_event_notifications.s(), name='add every 60', expires=10)


@app.task
def send_event_notifications():
    accounts = User.objects.values('id', 'email').all()
    for account in accounts:
        events = get_all_events_by_account(account['id'])
        email = account['email']

        send_mail(
            'Event notifications',
            'You have %s active events!' % (events.count()),
            'Memorio',
            [email],
            fail_silently=False,
        )


@app.task
def send_reset_password_mail(reset_code, email, site):
    send_mail(
        'Memorio password reset',
        'This is your reset code: %s\nLink to reset: %s/%s' % (reset_code, site, reset_code),
        'Memorio',
        [email],
        fail_silently=False,
    )
